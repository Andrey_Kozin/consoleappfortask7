﻿using System;

namespace ConsoleAppForTask7
{
    class Program
    {
        public static void Main(string[] args)
        {
            int[] num = {1, 32, 53, 44, 52, 6, 7, 8, 9, 0, 10, 22, 34, 523, 423,-1,-23,-142,-213123,-24};
            foreach (int v in num.TakeOdd())
            {
                Console.WriteLine(v);
            }
            Console.WriteLine("now we will show even");
            foreach (int v in num.TakeEven())
            {
                Console.WriteLine(v);
            }
            Console.WriteLine("no we will show Negative");
            foreach (int v in num.TakeNegative())
            {
                Console.WriteLine(v);
            }
            Console.WriteLine("no we will show Positive");
            foreach (int v in num.TakePositive())
            {
                Console.WriteLine(v);
            }
            Console.WriteLine("no we will show SmoothByMovingAverage");
            foreach (double v in num.SmoothByMovingAverage(3))
            {
                Console.WriteLine(v);
            }
        }
    }
}