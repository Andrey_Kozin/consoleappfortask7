﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleAppForTask7
{
    public static class LINQExtension
    {
        public static IEnumerable<int> TakeOdd(this IEnumerable<int> enumerable)
        {
            List<int> list = new List<int>();
            IEnumerable<int> filteringQuery =
                from num in enumerable
                where num % 2 != 0
                select num;

            return filteringQuery;
        }
        public static IEnumerable<int> TakeEven(this IEnumerable<int> enumerable)
        {
            List<int> list = new List<int>();
            IEnumerable<int> filteringQuery =
                from num in enumerable
                where num % 2 == 0
                select num;

            return filteringQuery;
        }
        public static IEnumerable<int> TakeNegative(this IEnumerable<int> enumerable)
        {
            List<int> list = new List<int>();
            IEnumerable<int> filteringQuery =
                from num in enumerable
                where num < 0
                select num;

            return filteringQuery;
        }
        public static IEnumerable<int> TakePositive(this IEnumerable<int> enumerable)
        {
            List<int> list = new List<int>();
            IEnumerable<int> filteringQuery =
                from num in enumerable
                where num > 0
                select num;

            return filteringQuery;
        }
        public static IEnumerable<double> SmoothByMovingAverage(this IEnumerable<int> enumerable, int width)
        {
            return Enumerable.Range(0,1+ enumerable.Count() - width).
                    Select(i => enumerable.Skip(i).Take(width).Average())
                    .ToList();
        }
    }
}